﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public int vel;
    private int hp = 3;
    public int fsalto;
    private bool puedeSaltar;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("d"))
        {
            //this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, 0);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));
            Debug.Log("La velocidad es " + this.GetComponent<Rigidbody2D>().velocity);
        }else if (Input.GetKey("a"))
        {
            //this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-vel, 0));
            Debug.Log("La velocidad es " + this.GetComponent<Rigidbody2D>().velocity);
        }

        if (Input.GetKey("w"))
        {
            if (puedeSaltar)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fsalto));
                puedeSaltar = false;
            }
        }
      

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Colisionando con " + collision.gameObject.name);
        puedeSaltar = true;

        if (collision.gameObject.CompareTag("Mortal"))
        {
            hp--;
            if (hp > 0)
            {
                if (this.GetComponent<Rigidbody2D>().velocity.x > 0.1)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-vel, 0));

                }else if (this.GetComponent<Rigidbody2D>().velocity.x > -0.1)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));
                }
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }


}
