﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fondo : MonoBehaviour
{
    //velocidad del scroll
    public float speedScroll = 0.5f;
    //velocidad del objeto fondo
    public float vel;
    Vector2 offset;
    public PlayerController player;

    

    // Start is called before the first frame update
    void Start()
    {
            //hacemos que el fondo avance a una velocidad
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
       
    }

    // Update is called once per frame
    void Update()
    {
        //hacemos que la imagen/textura del fondo se vaya moviendo hacia la izquierda
        offset = new Vector2(Time.time * speedScroll, 0);

        GetComponent<Renderer>().material.mainTextureOffset = offset;

        //cuando muere paramos el fondo
        if (!player.vida)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, 0);
        }


    }


}
