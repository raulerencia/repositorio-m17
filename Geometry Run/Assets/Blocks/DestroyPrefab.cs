﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPrefab : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //añadimos un tiempo de vida a los prefabs que se van formando
        GameObject.Destroy(gameObject, 20);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
