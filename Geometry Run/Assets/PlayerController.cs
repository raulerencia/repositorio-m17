﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    int points;
    public int vel;
    public int fsalto;
    public Text score;
    private bool puedeSaltar;
    private bool saltoAire;
    private bool InvGravity = false;
    public AudioClip pointSound;
    public bool vida = true;
    public GameObject quitarTexto;
    public GameObject GOtext;
    public GameObject GOscoretext;
    public Text GOscore;


    private void Awake()
    {
        //Inicializamos los puntos
        points = 0;
        ActScore();
    }

    //funcion para ir actualizando los puntos
    private void ActScore()
    {
        score.text = "Score: " + points;
        GOscore.text = "Score: " + points;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (vida)
        {
            //hacemos que el player avance hacia la derecha a una velocidad constante
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);

            if (Input.GetKey("w"))
            {
                //salto a mitad del aire
                if (saltoAire)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fsalto * 1.6f));
                    puedeSaltar = false;
                    saltoAire = false;
                    //salto con gravedad invertida
                }
                else if (puedeSaltar && InvGravity)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -fsalto));
                    puedeSaltar = false;
                }
                //salto colisionando con el suelo
                else if (puedeSaltar)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fsalto));
                    puedeSaltar = false;
                }
            }

            //actualizamos la puntuacion
            ActScore();
        }
        else
        {
            //iniciamos el texto de game over
            quitarTexto.SetActive(false);
            GOtext.SetActive(true);
            GOscoretext.SetActive(true);
            
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.transform.name);
        //cuando colisiona con el suelo podra saltar
        if (collision.gameObject.tag == "Suelo")
        {
            puedeSaltar = true;
        }
        //cuando colisiona con un obstaculo le quitamos la vida
        if (collision.transform.tag == "Obstacle")
        {
            vida = false;
            this.GetComponent<Transform>().Rotate(180, 0, 0);

            this.GetComponent<Rigidbody2D>().simulated = false;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //añadimos puntos al recoger monedas
        if(collision.tag == "Point")
        {
            points += 10;
            this.GetComponent<AudioSource>().PlayOneShot(pointSound);
            GameObject.Destroy(collision.gameObject);
        }
        //hacemos que pueda saltar en el aire en los puntos amarillos
        else if (collision.tag == "Salto")
        {
            puedeSaltar = true;
            saltoAire = true;

        }
        //hacemos que no pueda saltar al caer de las plataformas
        else if (collision.tag == "NoSalto")
        {
            puedeSaltar = false;
            saltoAire = false;
        }
        //invertimos la gravedad
        else if (collision.tag == "Invertir")
        {
            if (!InvGravity)
            {
                InvGravity = true;
            }else if (InvGravity)
            {
                InvGravity = false;
            }
            float gravedad = this.GetComponent<Rigidbody2D>().gravityScale;
            gravedad = gravedad * (-1);
            this.GetComponent<Rigidbody2D>().gravityScale = gravedad;

            //rotamos al personaje
            this.GetComponent<Transform>().Rotate(180, 0, 0);
            
        }
        //si no es nada de lo anterior se muere
        else
        {
            vida = false;
            this.GetComponent<Transform>().Rotate(180, 0, 0);

            this.GetComponent<Rigidbody2D>().simulated = false;
        }
        
    }



}

